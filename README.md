# Friends
This is a solution of challenge "Friends" writed by Schibsted, you ca read the requirement [here](https://github.com/scm-spain/ms-ma--backend-test)

## Implemented solution
Current solution is a API-REST implemented with spring-boot and documented with swagger.
- You can startup the api running ChallengeApplication.java and take a look the documentation in [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
- Current storage is supported by H2 in memory database.
- Response of any request with validation errors follow the [JsonAPI error standard](https://jsonapi.org/examples/#error-objects) 
- I have configured the checkstyle plugging. You can take a look at the config in config/checkstyle/**
- I have added a new gradle task  named "prePush". When you run it,  execute acceptance-Test task,integration-Test task, test task, checkstyle validation and build task. If you want to push something to repository, you should ejecute this task and check if it finish right

## Structure
You could find three tests folders:
- **acceptanceTest:** Contains tests end to end, It are written in junit using spring-rest-template
- **integrationTest:** Contains Integration tests of mvc layer and repository layer
- **test:** Contains unitary tests.



## Next Steps
- Implement user Accept and Decline friendship
- Implement user's friends fetching 
- Implement ForeverAlone badge with event-buss or some similar
- Create feign client 
- 
 
### Notes
**Interesting things:**
- All code has been developer in TDD.
- Clock interface to easier testing with time.
- PrePush task.
- Checkstyle!!
- MotherObject pattern.
-