package es.schibsted.challenge.interfaces;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.OK;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class RequestFriendshipAcceptanceTest {

    @Autowired
    @SuppressWarnings("unused")
    private TestRestTemplate restTemplate;

    private UserPublicDiver userPublicDiver;

    @Before
    public void setUp() {
        userPublicDiver = new UserPublicDiver(restTemplate);
    }

    @Test
    public void shouldReturnOkWhenBothUserExists() {

        //given
        final UserResource loggedUser = userPublicDiver.newUserWithDefaultPass("israel");
        final UserResource targetOfFriendship = userPublicDiver.newUserWithDefaultPass("jorge");

        //when
        final ResponseEntity<String> response = restTemplate
                .withBasicAuth(loggedUser.getUserName(), UserPublicDiver.DEFAULT_PASSWORD)
                .postForEntity("/users/" + loggedUser.getUserName() + "/friendships/" + targetOfFriendship.getUserName(), null, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(OK);
    }
}
