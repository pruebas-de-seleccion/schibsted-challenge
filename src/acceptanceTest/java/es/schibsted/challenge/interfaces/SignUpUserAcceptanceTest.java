package es.schibsted.challenge.interfaces;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class SignUpUserAcceptanceTest {

    @SuppressWarnings({"SpringAutowiredFieldsWarningInspection", "unused"})
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturnCreatedUserWhenCreateNewUser() {

        //given
        final CreateUserCommand userToCreate = new CreateUserCommand("Carmelo", "password");

        //when
        final ResponseEntity<UserResource> response = restTemplate.postForEntity("/users", userToCreate, UserResource.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getUserName())
                .as("Created user must be a correct username")
                .isEqualTo(userToCreate.getUserName());
    }


    //TODO  shouldAllowNewUserToRetrieveTheirFriendship() {

}
