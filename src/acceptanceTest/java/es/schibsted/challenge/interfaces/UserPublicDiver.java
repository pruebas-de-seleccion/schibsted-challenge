package es.schibsted.challenge.interfaces;

import lombok.AllArgsConstructor;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static lombok.AccessLevel.PACKAGE;

@AllArgsConstructor(access = PACKAGE)
class UserPublicDiver {

    static final String DEFAULT_PASSWORD = "password";

    private TestRestTemplate restTemplate;

    UserResource newUserWithDefaultPass(final String username) {

        final CreateUserCommand userToCreate = new CreateUserCommand(username, DEFAULT_PASSWORD);

        return restTemplate.postForEntity("/users", userToCreate, UserResource.class).getBody();
    }
}
