package es.schibsted.challenge.domain;

import java.time.Instant;
import java.util.Random;

public final class UserMother {

    private UserMother() {
    }

    public static User sample() {
        return User.builder()
                .id(new Random().nextLong())
                .userName("jorge")
                .password("password")
                .profileVisibility(ProfileVisibility.PUBLIC)
                .signUpDate(Instant.now())
                .build();
    }
}
