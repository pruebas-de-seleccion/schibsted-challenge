package es.schibsted.challenge.infrastructure.user;

final class UserEntityMother {

    private UserEntityMother() {
    }

    static UserEntity sample() {

        UserEntity sampleUser = new UserEntity();
        sampleUser.setUserName("JoseManuel");
        return sampleUser;
    }
}
