package es.schibsted.challenge.infrastructure.user;

import es.schibsted.challenge.domain.User;
import es.schibsted.challenge.domain.UserMother;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    private final UserEntityMapper mapper = new UserEntityMapper();

    @Autowired
    @SuppressWarnings("unused")
    private JpaUserRepository repository;

    private UserRepository userRepository;

    @Before
    public void setUp() {
        userRepository = new SpringDataUserRepository(repository, mapper);
    }

    @Test
    public void shouldReturnTrueWhenUserNameAlreadyExist() {

        //given
        final UserEntity storedUser = repository.save(UserEntityMother.sample());

        //when
        final Boolean exist = userRepository.existUserName(storedUser.getUserName());

        //then
        assertThat(exist).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenUserNameIsUnknown() {

        //given
        final String unknownUserName = "unknown";

        //when
        final Boolean exist = userRepository.existUserName(unknownUserName);

        //then
        assertThat(exist).isFalse();
    }

    @Test
    public void shouldStoreUser() {

        //given
        final User userToStore = UserMother.sample();
        final Boolean existNameBeforeStore = userRepository.existUserName(userToStore.getUserName());

        //when
        userRepository.store(userToStore);

        //then
        final Boolean existNameAfterStore = userRepository.existUserName(userToStore.getUserName());
        assertThat(existNameBeforeStore).isFalse();
        assertThat(existNameAfterStore).isTrue();
    }


    @Test
    public void shouldReturnSameUserWhenStoreUser() {

        //given
        final User userToStore = UserMother.sample();

        //when
        final User storedUser = userRepository.store(userToStore);

        //then
        assertThat(storedUser.getId()).isNotNull();
        assertThat(storedUser).isEqualToIgnoringGivenFields(userToStore, "id");
    }

}
