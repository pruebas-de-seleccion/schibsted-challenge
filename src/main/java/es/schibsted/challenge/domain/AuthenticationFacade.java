package es.schibsted.challenge.domain;

import es.schibsted.challenge.infrastructure.athorization.SecurityContextHolderFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class AuthenticationFacade {

    private final SecurityContextHolderFacade securityContextHolderFacade;

    public Optional<String> getCurrentAuthenticationUsername() {
        return Optional.ofNullable(securityContextHolderFacade.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getName)
                .filter(s -> !s.isEmpty());
    }
}
