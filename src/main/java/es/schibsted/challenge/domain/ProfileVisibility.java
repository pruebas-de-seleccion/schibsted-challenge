package es.schibsted.challenge.domain;

public enum ProfileVisibility {
    PUBLIC, HIDDEN
}
