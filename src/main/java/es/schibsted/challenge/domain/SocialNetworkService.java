package es.schibsted.challenge.domain;

import es.schibsted.challenge.infrastructure.Clock;
import es.schibsted.challenge.infrastructure.user.UserRepository;
import es.schibsted.challenge.interfaces.CreateUserCommand;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static es.schibsted.challenge.domain.ProfileVisibility.PUBLIC;

@Service
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class SocialNetworkService {

    private final UserRepository userRepository;
    private final Clock clock;

    public Optional<User> signUpNewUser(final CreateUserCommand createCommand) {

        if (userRepository.existUserName(createCommand.getUserName())) {
            return Optional.empty();
        }

        return Optional.of(userRepository.store(User.builder()
                .userName(createCommand.getUserName())
                .password(createCommand.getPassword())
                .signUpDate(clock.getCurrentDate())
                .profileVisibility(PUBLIC)
                .build()));
    }
}
