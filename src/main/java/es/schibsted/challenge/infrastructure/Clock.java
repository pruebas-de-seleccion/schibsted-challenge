package es.schibsted.challenge.infrastructure;

import java.time.Instant;

public interface Clock {

    Instant getCurrentDate();
}
