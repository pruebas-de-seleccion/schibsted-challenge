package es.schibsted.challenge.infrastructure;

import com.google.common.base.Predicates;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.Instant;

/**
 * Enables the Swagger API documentation.
 */
@Configuration
@EnableSwagger2
@ConditionalOnProperty(prefix = "feature-toggle.swagger2", name = "enabled", havingValue = "true")
class SwaggerConfiguration {

    @Bean
    public Docket api() {
        //noinspection Guava
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                //Exclude of documentation all the spring base controller like /errors
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .paths(PathSelectors.any())
                .build()
                .directModelSubstitute(Instant.class, String.class);
    }
}
