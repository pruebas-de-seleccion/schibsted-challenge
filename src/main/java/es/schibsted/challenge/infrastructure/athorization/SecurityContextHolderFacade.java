package es.schibsted.challenge.infrastructure.athorization;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityContextHolderFacade {

    public SecurityContext getContext() {
        return SecurityContextHolder.getContext();
    }
}
