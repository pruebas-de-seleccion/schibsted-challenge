package es.schibsted.challenge.infrastructure.user;

import es.schibsted.challenge.domain.User;
import org.springframework.stereotype.Component;

@Component
class UserEntityMapper {

    UserEntity map(final User source) {

        final UserEntity target = new UserEntity();
        target.setId(source.getId());
        target.setUserName(source.getUserName());
        target.setPassword(source.getPassword());
        target.setProfileVisibility(source.getProfileVisibility());
        target.setSignUpDate(source.getSignUpDate());
        return target;
    }

    User map(final UserEntity source) {

        return User.builder()
                .id(source.getId())
                .userName(source.getUserName())
                .password(new String(source.getPassword()))
                .profileVisibility(source.getProfileVisibility())
                .signUpDate(source.getSignUpDate())
                .build();
    }
}
