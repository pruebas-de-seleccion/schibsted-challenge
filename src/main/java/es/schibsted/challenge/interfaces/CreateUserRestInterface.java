package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.domain.SocialNetworkService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static lombok.AccessLevel.PACKAGE;
import static org.springframework.http.HttpStatus.CONFLICT;

@RestController
@AllArgsConstructor(access = PACKAGE)
class CreateUserRestInterface {
    private final SocialNetworkService socialNetworkService;
    private final UserResourceMapper mapper;

    @PostMapping("/users")
    ResponseEntity<UserResource> createUser(@RequestBody @Valid final CreateUserCommand creationCommand) {
        return socialNetworkService.signUpNewUser(creationCommand)
                .map(mapper::map)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(CONFLICT).build());
    }
}
