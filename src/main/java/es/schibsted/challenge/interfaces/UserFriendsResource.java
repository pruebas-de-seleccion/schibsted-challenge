package es.schibsted.challenge.interfaces;

import lombok.Getter;

import java.util.Collection;

import static java.util.Collections.emptyList;

@Getter
final class UserFriendsResource {
    static final UserFriendsResource NO_FRIENDS = new UserFriendsResource();

    private final Collection<Object> friends;

    private UserFriendsResource() {
        friends = emptyList();
    }
}
