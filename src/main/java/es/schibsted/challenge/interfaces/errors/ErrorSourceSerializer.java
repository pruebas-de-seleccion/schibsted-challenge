package es.schibsted.challenge.interfaces.errors;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;


class ErrorSourceSerializer extends StdSerializer<String> {

    private static final String FIELD_POINTER = "pointer";

    @SuppressWarnings("unused")
    ErrorSourceSerializer() {

        this(null);
    }

    @SuppressWarnings("WeakerAccess")
    ErrorSourceSerializer(final Class<String> cause) {

        super(cause);
    }

    @Override
    public void serialize(final String sourceValue, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider) throws IOException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField(FIELD_POINTER, sourceValue);
        jsonGenerator.writeEndObject();
    }
}
