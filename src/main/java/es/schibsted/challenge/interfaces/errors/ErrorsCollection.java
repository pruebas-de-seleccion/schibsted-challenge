package es.schibsted.challenge.interfaces.errors;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.Collection;

@AllArgsConstructor
class ErrorsCollection implements Serializable {

    @JsonProperty("errors")
    private Collection<ErrorResource> errors;
}
