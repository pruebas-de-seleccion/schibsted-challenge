package es.schibsted.challenge.interfaces.errors;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ValidationErrorsHandler {


    @SuppressWarnings("unused")
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody
    ResponseEntity handleValidationException(final MethodArgumentNotValidException exception) {

        final List<ErrorResource> validationErrors = exception.getBindingResult().getAllErrors()
                .stream()
                .map(this::toDocumentValidationErrorDTO)
                .collect(Collectors.toList());

        return new ResponseEntity<>(new ErrorsCollection(validationErrors), HttpStatus.BAD_REQUEST);
    }

    private ErrorResource toDocumentValidationErrorDTO(final ObjectError objectError) {

        if (objectError instanceof FieldError) {
            return parseFieldError((FieldError) objectError);
        } else {
            return parseOtherError(objectError);
        }
    }

    private ErrorResource parseOtherError(final ObjectError objectError) {

        return ErrorResource.builder()
                .sourcePointer(Arrays.toString(objectError.getCodes()))
                .code(objectError.getCode())
                .title(objectError.getDefaultMessage())
                .build();
    }

    private ErrorResource parseFieldError(final FieldError objectError) {

        return ErrorResource.builder()
                .sourcePointer(objectError.getField())
                .code(objectError.getCode() + "." + objectError.getField())
                .title(objectError.getDefaultMessage())
                .build();
    }
}

