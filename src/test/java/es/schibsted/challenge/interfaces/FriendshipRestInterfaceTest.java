package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.domain.AuthenticationFacade;
import es.schibsted.challenge.domain.Friendship;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FriendshipRestInterfaceTest {

    private final FriendshipService friendshipService = mock(FriendshipService.class);
    private final AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);

    private FriendshipRestInterface friendshipRestInterface;

    @Before
    public void beforeTest() {

        friendshipRestInterface = new FriendshipRestInterface(friendshipService, authenticationFacade);
    }


    @Test
    public void shouldReturnUnauthorizedWhenThereAreNotAuthenticatedUser() {

        //when
        final ResponseEntity<Friendship> response = friendshipRestInterface.createFriendship("agustin", "rafa");

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void shouldReturnForbiddenWhenSourceUserAndAuthenticatedUserNotAreTheSame() {

        //given
        authenticateUser("matias");

        //when
        final ResponseEntity<Friendship> response = friendshipRestInterface.createFriendship("lucas", "pablo");

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void shouldReturnNotFoundWhenServiceReturnEmpty() {

        //given
        final String sourceUsername = "maria";
        final String targetUsername = "roberto";
        authenticateUser(sourceUsername);

        when(friendshipService.createFriendship(sourceUsername, targetUsername)).thenReturn(Optional.empty());

        //when
        final ResponseEntity<Friendship> response = friendshipRestInterface.createFriendship(sourceUsername, targetUsername);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }


    @Test
    public void shouldReturnOkWhenServiceCreateAFriendship() {

        //given
        final String sourceUser = "isabel";
        final String targetUser = "manuel";
        authenticateUser(sourceUser);

        when(friendshipService.createFriendship(sourceUser, targetUser)).thenReturn(Optional.of(Friendship.builder().build()));

        //when
        final ResponseEntity<Friendship> response = friendshipRestInterface.createFriendship(sourceUser, targetUser);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void authenticateUser(final String username) {
        when(authenticationFacade.getCurrentAuthenticationUsername()).thenReturn(Optional.of(username));
    }
}
